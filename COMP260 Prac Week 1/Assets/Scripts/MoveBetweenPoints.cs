﻿using UnityEngine;
using System.Collections;

public class MoveBetweenPoints : MonoBehaviour
{
    public Vector3 startPoint;
    public Vector3 endPoint;
    public float speed = 1.0f;

    private bool movingForward = true;

    private void Start(){
        transform.position = startPoint;
    }
    void Update (){
        Vector3 target;
        if (movingForward)
        {
            target = endPoint;
        } else {
            target = startPoint;
        }
        float distanceToMove = speed * Time.deltaTime;
        float distanceToTarget = (target - transform.position).magnitude;

        if (distanceToMove > distanceToTarget)
        {
            transform.position = target;
            movingForward = !movingForward;
        }
        else
        {
            Vector3 dir = (target - transform.position).normalized;
            transform.position += dir * distanceToMove;
        }
        if (distanceToMove > 0)        {
            print(distanceToMove);
        }
        }

    }

